<?php

declare(strict_types=1);

namespace Serganbus\StatementParser;

use DateTimeInterface;
use Money\Money;

class OutcomeOperation extends Operation
{
    public function __construct(
        DateTimeInterface $datetime,
        Money $amount,
        string $description,
        string $bank,
        string $accountNumber,
        public readonly ?string $mccCode = null,
        public readonly ?string $terminalName = null,
    ) {
        parent::__construct($datetime, $amount, $description, $bank, $accountNumber);
    }
}
