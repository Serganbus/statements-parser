<?php

declare(strict_types=1);

namespace Serganbus\StatementParser\Parsers;

use Smalot\PdfParser\Config;
use Smalot\PdfParser\Page;
use Smalot\PdfParser\Parser as PdfParser;
use Serganbus\StatementParser\ParserInterface;
use Serganbus\StatementParser\IncomeOperation;
use Serganbus\StatementParser\Operation;
use Serganbus\StatementParser\OutcomeOperation;
use Money\Money;

/**
 * Парсер выписок с операциями по счету банка ВТБ
 *
 * @author serganbus
 */
class VtbParser implements ParserInterface
{
    public const BANK_NAME = 'ВТБ';

    /**
     * @param array<string, string> $comparison
     * @return array<string, string>
     */
    public static function normalizeTerminalAndMccComparison(array $comparison): array
    {
        uksort($comparison, function (string $first, string $second) {
            $firstLen = mb_strlen($first);
            $secondLen = mb_strlen($second);
            if ($firstLen < $secondLen) {
                return 1;
            }
            return ($firstLen === $secondLen) ? 0 : -1;
        });

        return $comparison;
    }

    /**
     * @var array<string, string> $terminalsToMcc
     */
    private array $terminalsToMcc;

    /**
     * @param array<string, string> $terminalsToMcc
     */
    public function __construct(array $terminalsToMcc = [])
    {
        $this->terminalsToMcc = $terminalsToMcc;
    }

    public function getBankName(): string
    {
        return self::BANK_NAME;
    }

    public function parse(string $filePath): array
    {
        $realPathFrom = realpath($filePath);
        if ($realPathFrom === false || !is_readable($realPathFrom)) {
            throw new \InvalidArgumentException("File is not readable. File: '$filePath'");
        }

        $fileContent = file_get_contents($realPathFrom);
        if ($fileContent === false) {
            throw new \RuntimeException("Не удалось прочитать файл '$realPathFrom'.");
        }

        $pdf = $this->getParser()->parseFile($realPathFrom);

        // Извлекаем все страницы из PDF файла
        $pages = $pdf->getPages();

        if (!preg_match('/\s+(\d{20})\s+/', $pages[0]->getText(), $matches)) {
            throw new \RuntimeException("Номер счета не найден.");
        }
        $accountNumber = $matches[1];

        $parsedStatement = [];
        // проходимся по каждой странице и получаем текст
        foreach ($pages as $page) {
            /** @var Page $page */
            $parsedPage = $this->parsePage($page->getText());
            $parsedStatement = array_merge($parsedStatement, $parsedPage);
        }

        $operations = [];
        foreach ($parsedStatement as $operationInfo) {
            $operations[] = $this->getOperation($operationInfo, $accountNumber);
        }

        return $operations;
    }

    /**
     *
     * @param array<string> $operationInfo
     * @param string $accountNumber
     * @return Operation
     * @throws \RuntimeException
     */
    private function getOperation(array $operationInfo, string $accountNumber): Operation
    {
        $dateTime = \DateTime::createFromFormat('d.m.YH:i:s', $operationInfo[1]);
        if (!$dateTime) {
            throw new \RuntimeException("Invalid datetime: $operationInfo[1]");
        }
        [$amountStr, $currency] = explode(' ', $operationInfo[3]);
        $amount = Money::$currency((int) ((float) $amountStr * 100));
        $description = trim($operationInfo[7]);
        $isIncome = (float) $operationInfo[4] > 0;
        if ($isIncome) {
            return new IncomeOperation(
                datetime: $dateTime,
                amount: $amount,
                description: $description,
                bank: self::BANK_NAME,
                accountNumber: $accountNumber,
            );
        }

        $terminalName = null;
        $mccCode = null;
        foreach ($this->terminalsToMcc as $terminalNameCompare => $mccCodeCompare) {
            if (!str_contains($description, $terminalNameCompare)) {
                continue;
            }

            $terminalName = $terminalNameCompare;
            $mccCode = $mccCodeCompare;

            break;
        }

        return new OutcomeOperation(
            datetime: $dateTime,
            amount: $amount,
            description: $description,
            bank: self::BANK_NAME,
            accountNumber: $accountNumber,
            mccCode: $mccCode,
            terminalName: $terminalName,
        );
    }

    private function getParser(): PdfParser
    {
        $config = new Config();
        // An empty string can prevent words from breaking up
        //$config->setHorizontalOffset(' ');
        // A tab can help preserve the structure of your document
        //$config->setHorizontalOffset("\t");
        return new PdfParser([], $config);
    }

    /**
     * @return array<array<string>>
     * @throws \RuntimeException
     */
    private function parsePage(string $pageData): array
    {
        $parsedPage = [];

        preg_match_all('/^\d{2}\.\d{2}\.\d{6}:\d{2}:\d{2}.+$/m', $pageData, $matches);

        foreach ($matches[0] as $line) {
            $parsedLine = $this->parseLine($line);
            $parsedPage[] = $parsedLine;
        }

        return $parsedPage;
    }

    /**
     * @return array<string>
     * @throws \RuntimeException
     */
    private function parseLine(string $lineData): array
    {
        // видимо, старый формат предоставления выписок
        $regexpOld = '/^(\d{2}\.\d{2}\.\d{6}:\d{2}:\d{2})\s+(\d{2}\.\d{2}\.\d{4})\s+([\.\d]+ RUB|EUR|USD)\s+([\.\d]+)\s+([\.\d]+)\s+([\.\d]+)\s*(\D+.+)$/m';
        $matched = preg_match($regexpOld, $lineData, $matches);
        if ($matched) {
            return $matches;
        }

        // видимо, новый формат предоставления выписок
        $regexpNew = '/^(\d{2}\.\d{2}\.\d{6}:\d{2}:\d{2})\s+(\d{2}\.\d{2}\.\d{4})\s+([\.\d]+ RUB|EUR|USD)\s+([\.\d]+)\s+([\.\d]+)\s*(\D+.+)$/m';
        $matched = preg_match($regexpNew, $lineData, $matches);
        if ($matched) {
            $description = $matches[6];
            $matches[6] = '0'; // остаток задолженности
            $matches[7] = $description;
            return $matches;
        }

        throw new \RuntimeException("Line cannot be parsed. Line: $lineData");
    }
}
