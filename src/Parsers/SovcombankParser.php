<?php

declare(strict_types=1);

namespace Serganbus\StatementParser\Parsers;

use Serganbus\StatementParser\ParserInterface;
use Serganbus\StatementParser\Operation;
use Serganbus\StatementParser\IncomeOperation;
use Serganbus\StatementParser\OutcomeOperation;
use Money\Money;

/**
 * Парсер выписок с операциями по счету банка Совкомбанк
 *
 * @author serganbus
 */
class SovcombankParser implements ParserInterface
{
    public const BANK_NAME = 'Совкомбанк';

    public function getBankName(): string
    {
        return self::BANK_NAME;
    }

    public function parse(string $filePath): array
    {
        $realPathFrom = realpath($filePath);
        if ($realPathFrom === false || !is_readable($realPathFrom)) {
            throw new \InvalidArgumentException("File is not readable. File: '$filePath'");
        }

        $fileContent = file_get_contents($realPathFrom);
        if ($fileContent === false) {
            throw new \RuntimeException("Файл '$realPathFrom' не может быть прочитан.");
        }

        if (!preg_match('/Выписка по счету\s+(\d{20})/', $fileContent, $matches)) {
            throw new \RuntimeException("Номер счета не найден.");
        }
        $accountNumber = $matches[1];

        $doc = new \DOMDocument();
        $doc->loadHTML($fileContent);

        $xpath = new \DOMXPath($doc);
        $xPathResult = $xpath->query('//table[2]/tbody/tr');
        if ($xPathResult === false) {
            throw new \RuntimeException("Не найдены записи выписки.");
        }
        $i = 0;
        $transactions = [];
        foreach ($xPathResult as $childNode) {
            /** @var \DOMNode $childNode */
            $i++;

            if ($i === 1) {
                // заголовки отбрасываем
                continue;
            }

            $transactions[] = $this->parseOperation($childNode, $xpath, $accountNumber);
        }

        return $transactions;
    }

    private function parseOperation(\DOMNode $childNode, \DOMXPath $xpath, string $accountNumber): Operation
    {
        $tdChildsQuery = $xpath->query('td', $childNode);
        if ($tdChildsQuery === false) {
            throw new \RuntimeException("Не найдены ячейки в строке таблицы.");
        }
        $dateChild = $tdChildsQuery->item(0);
        if ($dateChild === null) {
            throw new \RuntimeException("Не найдена ячейка с датой.");
        }
        $debitChild = $tdChildsQuery->item(3);
        if ($debitChild === null) {
            throw new \RuntimeException("Не найдена ячейка с дебитом.");
        }
        $kreditChild = $tdChildsQuery->item(4);
        if ($kreditChild === null) {
            throw new \RuntimeException("Не найдена ячейка с кредитом.");
        }
        $descriptionChild = $tdChildsQuery->item(5);
        if ($descriptionChild === null) {
            throw new \RuntimeException("Не найдена ячейка с описанием.");
        }

        $dateRaw = trim($dateChild->textContent);
        $dateTime = \DateTime::createFromFormat('d.m.y', $dateRaw);
        if (!$dateTime) {
            throw new \RuntimeException("Invalid datetime: $dateRaw");
        }
        $dateTime->setTime(0, 0, 0, 0);

        $amount = $this->toMoney(trim($debitChild->textContent));
        if ($amount->isZero()) {
            // пополнение
            $amount = $this->toMoney(trim($kreditChild->textContent));

            return new IncomeOperation(
                datetime: $dateTime,
                amount: $amount,
                description: trim($descriptionChild->textContent),
                bank: self::BANK_NAME,
                accountNumber: $accountNumber,
            );
        }

        // платеж
        $description = trim($descriptionChild->textContent);

        $mccCode = null;
        if (preg_match('/.*MCC\s+(\d{3,4}).*/', $description, $matches)) {
            $mccCode = $matches[1];
        }

        $terminalName = null;
        if (strtoupper(mb_substr($description, 0, 6)) === 'ПЛАТЕЖ') {
            $explodedDescription = explode(',', $description);
            $endPart = end($explodedDescription);
            $explodedEndPart = explode('\\', $endPart);
            $terminalName = $explodedEndPart[count($explodedEndPart) - 2];

            // у платежа есть еще и время совершения платежа. Пробуем скорректировать дату и время операции
            $dateTimeCorrected = \DateTime::createFromFormat('d.m.y H:i', $explodedDescription[1]);
            if ($dateTimeCorrected instanceof \DateTime) {
                $dateTime = $dateTimeCorrected;
            }
        }

        return new OutcomeOperation(
            datetime: $dateTime,
            amount: $amount,
            description: $description,
            bank: self::BANK_NAME,
            accountNumber: $accountNumber,
            mccCode: $mccCode,
            terminalName: $terminalName,
        );
    }

    private function toMoney(string $notFormatted): Money
    {
        $amountStr = str_replace(',', '', $notFormatted);
        return Money::RUB((int) ((float) $amountStr * 100));
    }
}
