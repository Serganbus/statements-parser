<?php

declare(strict_types=1);

namespace Serganbus\StatementParser\Parsers;

use Smalot\PdfParser\Config;
use Smalot\PdfParser\Page;
use Smalot\PdfParser\Parser as PdfParser;
use Serganbus\StatementParser\ParserInterface;
use Serganbus\StatementParser\Operation;
use Serganbus\StatementParser\IncomeOperation;
use Serganbus\StatementParser\OutcomeOperation;
use Money\Money;

/**
 * Парсер выписок с операциями по счету банка АльфаБанк
 *
 * @author serganbus
 */
class AlfabankParser implements ParserInterface
{
    public const BANK_NAME = 'Альфа Банк';

    public function getBankName(): string
    {
        return self::BANK_NAME;
    }

    public function parse(string $filePath): array
    {
        $realPathFrom = realpath($filePath);
        if ($realPathFrom === false || !is_readable($realPathFrom)) {
            throw new \InvalidArgumentException("File is not readable. File: '$filePath'");
        }

        $fileContent = file_get_contents($realPathFrom);
        if ($fileContent === false) {
            throw new \RuntimeException("Файл '$realPathFrom' не может быть прочитан.");
        }

        $pdf = $this->getParser()->parseFile($realPathFrom);

        // Извлекаем все страницы из PDF файла
        $pages = $pdf->getPages();
        if (!preg_match('/\s+(\d{20})\s+/', $pages[0]->getText(), $matches)) {
            throw new \RuntimeException("Номер счета не найден.");
        }
        $accountNumber = $matches[1];

        if (!preg_match('/Валюта\s+счета\s+([A-Z]{3})/', $pages[0]->getText(), $matches)) {
            throw new \RuntimeException("валюта счета не найдена.");
        }
        $currency = $matches[1];

        $unparsedOperations = [];
        // проходимся по каждой странице и получаем текст
        foreach ($pages as $page) {
            /** @var Page $page */
            $parsedPage = $this->parsePage($page->getText());
            $unparsedOperations = array_merge($unparsedOperations, $parsedPage);
        }

        $operations = [];
        foreach ($unparsedOperations as $unparsedOPeration) {
            $operations[] = $this->getOperation($unparsedOPeration, $accountNumber, $currency);
        }

        return $operations;
    }

    /**
     *
     * @param string $unparsedOPeration
     * @param string $accountNumber
     * @return Operation
     * @throws \RuntimeException
     */
    private function getOperation(string $unparsedOPeration, string $accountNumber, string $currency): Operation
    {
        $dateStr = substr($unparsedOPeration, 0, 10);
        $dateTime = \DateTime::createFromFormat('d.m.Y', $dateStr);
        if (!$dateTime) {
            throw new \RuntimeException("Invalid datetime: $dateStr");
        }
        $exploded = explode("\t", $unparsedOPeration);
        $amountStrRaw = trim($exploded[3]);
        $amountStrWithCurrency = str_replace([' ', ','], ['', '.'], $amountStrRaw);
        $currencyPosition = mb_strpos($amountStrWithCurrency, $currency);
        $amountStr = '0';
        if ($currencyPosition !== false) {
            $amountStr = trim(mb_substr($amountStrWithCurrency, 0, $currencyPosition));
        }
        if ($currency === 'RUR') {
            $currency = 'RUB';
        }

        /** @var Money $amount */
        $amount = Money::$currency((int) ((float) $amountStr * 100));
        $description = $exploded[2];
        $isIncome = $amount->isPositive();
        if ($isIncome) {
            return new IncomeOperation(
                datetime: $dateTime,
                amount: $amount->absolute(),
                description: $description,
                bank: self::BANK_NAME,
                accountNumber: $accountNumber,
            );
        }

        $terminalName = null;
        $mccCode = null;
        if (preg_match('/MCC(\d{4})/', $unparsedOPeration, $matches)) {
            $mccCode = $matches[1];
        }

        return new OutcomeOperation(
            datetime: $dateTime,
            amount: $amount->absolute(),
            description: $description,
            bank: self::BANK_NAME,
            accountNumber: $accountNumber,
            mccCode: $mccCode,
            terminalName: $terminalName,
        );
    }

    private function getParser(): PdfParser
    {
        $config = new Config();
        // An empty string can prevent words from breaking up
        //$config->setHorizontalOffset(' ');
        // A tab can help preserve the structure of your document
        //$config->setHorizontalOffset("\t");
        return new PdfParser([], $config);
    }

    /**
     * @return array<string>
     * @throws \RuntimeException
     */
    private function parsePage(string $pageData): array
    {
        $parsedPage = [];

        preg_match_all('/Сумма\s+в валюте счета/m', $pageData, $matches);
        $strPos = mb_strpos($pageData, $matches[0][0]);
        $strLen = mb_strlen($matches[0][0]);
        $needle = mb_substr($pageData, $strPos + $strLen);

        preg_match_all('/^\d{2}\.\d{2}\.\d{4}.+/m', $needle, $matches);

        $operationsCount = count($matches[0]);
        $prevPosition = (int)($strPos + $strLen);
        for ($i = 1; $i <= $operationsCount; $i++) {
            if ($i === $operationsCount) {
                $lastPosition = mb_strpos($pageData, 'Страница');
            } else {
                $line = $matches[0][$i];
                $lineExploded = explode("\t", $line);
                $toSearch = "$lineExploded[0]\t$lineExploded[1]";
                $lastPosition = mb_strpos($pageData, $toSearch);
            }
            $length = $lastPosition - $prevPosition;
            $operationUnparsed = mb_substr($pageData, $prevPosition, $length);
            $parsedPage[] = trim($operationUnparsed);

            $prevPosition = (int)$lastPosition;
        }

        return $parsedPage;
    }
}
