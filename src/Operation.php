<?php

declare(strict_types=1);

namespace Serganbus\StatementParser;

use DateTimeInterface;
use Money\Money;

class Operation
{
    public function __construct(
        public readonly DateTimeInterface $datetime,
        public readonly Money $amount,
        public readonly string $description,
        public readonly string $bank,
        public readonly string $accountNumber,
    ) {
    }
}
