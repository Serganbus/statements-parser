<?php

declare(strict_types=1);

namespace Serganbus\StatementParser;

use Serganbus\StatementParser\Operation;

/**
 * @author serganbus
 */
interface ParserInterface
{
    /**
     * Парсер какого банка
     */
    public function getBankName(): string;

    /**
     * Метод, возвращающий массив операций
     *
     * @return Operation[]
     */
    public function parse(string $filePath): array;
}
