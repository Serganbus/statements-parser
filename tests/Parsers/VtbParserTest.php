<?php

declare(strict_types=1);

namespace Serganbus\StatementParser\Parsers;

use PHPUnit\Framework\TestCase;
use Serganbus\StatementParser\Operation;
use Serganbus\StatementParser\OutcomeOperation;

/**
 * @author serganbus
 */
class VtbParserTest extends TestCase
{
    protected array $terminals = [
        'MONETKA' => '5411',
        'ZHIZNMART' => '5411',
        'MAGNIT MK' => '5411',
        'MAGNIT MM' => '5411',
        'PYATEROCHKA' => '5411',
        'PEREKRESTOK' => '5411',
    ];

    protected ?VtbParser $parser;

    protected function setUp(): void
    {
        $this->parser = new VtbParser($this->terminals);
    }

    public function testParseOk()
    {
        $availableTerminalNames = array_merge(array_keys($this->terminals), [null]);
        $operations = $this->parser->parse(__DIR__ . '/../data/vtb-test-statement.pdf');
        foreach ($operations as $operation) {
            $this->assertInstanceOf(Operation::class, $operation);
            $this->assertEquals(VtbParser::BANK_NAME, $operation->bank);
            $this->assertEquals('40817810999999999999', $operation->accountNumber);
            $this->assertFalse($operation->amount->isNegative());
            if ($operation instanceof OutcomeOperation) {
                $this->assertTrue(in_array($operation->mccCode, [null, '5411']));
                $this->assertContains($operation->terminalName, $availableTerminalNames);
            }
        }
    }

    public function testParseRuntimeException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->parser->parse(__DIR__ . '/not-found.pdf');
    }

    public function testGetBankName()
    {
        $this->assertEquals(VtbParser::BANK_NAME, $this->parser->getBankName());
    }

    protected function tearDown(): void
    {
        $this->parser = null;
    }
}
