<?php

declare(strict_types=1);

namespace Serganbus\StatementParser\Parsers;

use PHPUnit\Framework\TestCase;
use Serganbus\StatementParser\Operation;
use Serganbus\StatementParser\OutcomeOperation;

/**
 * @author serganbus
 */
class SovcombankParserTest extends TestCase
{
    protected ?SovcombankParser $parser;

    protected function setUp(): void
    {
        $this->parser = new SovcombankParser();
    }

    public function testParseOk()
    {
        $availableTerminalNames = [null, '6513'];
        $operations = $this->parser->parse(__DIR__ . '/../data/sovcombank-test-statement.html');
        foreach ($operations as $operation) {
            $this->assertInstanceOf(Operation::class, $operation);
            $this->assertEquals(SovcombankParser::BANK_NAME, $operation->bank);
            $this->assertEquals('40817810999999999999', $operation->accountNumber);
            $this->assertFalse($operation->amount->isNegative());
            if ($operation instanceof OutcomeOperation) {
                $this->assertTrue(in_array($operation->mccCode, [null, '6513', '5300', '5964', '8398']));
            }
        }
    }

    public function testParseRuntimeException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->parser->parse(__DIR__ . '/not-found.html');
    }

    public function testGetBankName()
    {
        $this->assertEquals(SovcombankParser::BANK_NAME, $this->parser->getBankName());
    }

    protected function tearDown(): void
    {
        $this->parser = null;
    }
}
